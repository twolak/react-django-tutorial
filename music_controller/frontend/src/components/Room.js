import { Button, Grid, Link, Typography } from "@material-ui/core";
import React, { Component } from "react";
import CreateRoomPage from "./CreateRoomPage";

export default class Room extends Component {
  constructor(props) {
    super(props);
    this.state = {
      votesToSkip: 2,
      guestCanPause: false,
      isHost: false,
      showSettings: false,
      spotifyAuthenticated: false
    };
    this.roomCode = this.props.match.params.roomCode;
    this.getRoomDetails();
  }

  getRoomDetails = () => {
    console.log(this.roomCode)
    fetch("/api/get-room" + "?code=" + this.roomCode)
      .then((response) => {
        if (!response.ok) {
          this.props.leaveRoomCallback();
          this.props.history.push("/");
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          votesToSkip: data.votes_to_skip,
          guestCanPause: data.guest_can_pause,
          isHost: data.is_host,
        });
        if(this.state.isHost) {
          this.authenticateSpotify();
        }
      });
  };

  authenticateSpotify = () => {
    fetch('/spotify/is-authenticated')
    .then(response => response.json())
    .then(data => {
      this.setState({ spotifyAuthenticated: data.status });
      if (!data.status) {
        fetch('/spotify/get-auth-url').then(response => response.json())
        .then(data => {
          window.location.replace(data.url);
        });
      }
    });
  }

  leaveButtonPressed = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application-json" },
    };
    fetch("/api/leave-room", requestOptions).then((_response) => {
      this.props.leaveRoomCallback();
      this.props.history.push("/");
    });
  };

  updateShowSettings = value => {
    this.setState({
      showSettings: value
    });
  }

  renderSettingsButton = () => {
    return (
      <Grid item xs={12} align="center">
        <Button variant="contained" color="primary" onClick={() => this.updateShowSettings(true)}>
          Settings
        </Button>
      </Grid>
    );
  }

  renderSettings = () => {
    const { votesToSkip, guestCanPause } = this.state;

    return (<Grid item xs={12} align="center">
      <Grid item xs={12} align="center">
        <CreateRoomPage 
          update={true} 
          votesToSkip={votesToSkip}
          guestCanPause={guestCanPause} 
          roomCode={this.roomCode} 
          updateCallback={this.getRoomDetails}/>
      </Grid>
      <Grid item xs={12} align="center">
      <Button
            color="secondary"
            variant="contained"
            onClick={() => this.updateShowSettings(false)}
          >
            Close
          </Button>
      </Grid>
    </Grid>);
  }

  render() {
    const { showSettings } = this.state;

    if (showSettings) {
      return this.renderSettings();
    }
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} align="center">
          <Typography variant="h4" component="h4">
            Code: {this.roomCode}
          </Typography>
        </Grid>
        <Grid item xs={12} align="center">
          <Typography variant="h6" component="h6">
            Votes: {this.state.votesToSkip}
          </Typography>
        </Grid>
        <Grid item xs={12} align="center">
          <Typography variant="h6" component="h6">
            Guest Can Pause: {this.state.guestCanPause.toString()}
          </Typography>
        </Grid>
        <Grid item xs={12} align="center">
          <Typography variant="h6" component="h6">
            Is Host: {this.state.isHost.toString()}
          </Typography>
        </Grid>
        {this.state.isHost ? this.renderSettingsButton() : null}
        <Grid item xs={12} align="center">
          <Button
            color="secondary"
            variant="contained"
            onClick={this.leaveButtonPressed}
          >
            Leave Room
          </Button>
        </Grid>
      </Grid>
    );
  }
}
/* 
    <div>
        <h3>{this.roomCode}</h3>
        <p>Votes: {this.state.votesToSkip}</p>
        <p>Guest Can Pause: {this.state.guestCanPause.toString()}</p>
        <p>Is Host: {this.state.isHost.toString()}</p>
    </div>
 */
