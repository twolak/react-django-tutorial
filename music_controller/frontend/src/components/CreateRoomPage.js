import React, { Component } from "react";
import {
  Button,
  Grid,
  Typography,
  TextField,
  FormHelperText,
  FormControl,
  Radio,
  RadioGroup,
  FormControlLabel,
  Collapse
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Alert }from "@material-ui/lab";

export default class CreateRoomPage extends Component {
  static defaultProps = {
    votesToSkip: 2,
    guestCanPause: true,
    update: false,
    roomCode: null,
    updateCallback: () => { },
  };

  constructor(props) {
    super(props);
    this.state = {
      guestCanPause: this.props.guestCanPause,
      votesToSkip: this.props.votesToSkip,
      successMsg: "",
      errorMsg: "",
    };
  }
  handleVotesChange = (e) => {
    this.setState({
      votesToSkip: e.target.value,
    });
  };

  handleGuestCanPauseChange = (e) => {
    this.setState({
      guestCanPause: e.target.value === "true" ? true : false,
    });
  };

  handleCreateButtonPressed = (e) => {
    const { votesToSkip, guestCanPause } = this.state;
    console.log(votesToSkip, guestCanPause, this.props.history);
    const requestOptions = {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        votes_to_skip: votesToSkip,
        guest_can_pause: guestCanPause,
      }),
    };
    fetch("/api/create-room", requestOptions)
      .then((response) => response.json())
      .then((data) => this.props.history.push("/room/" + data.code));
  };

  handleUpdateButtonPressed = () => {
    const { votesToSkip, guestCanPause } = this.state;
    const { roomCode, updateCallback } = this.props;

    const requestOptions = {
      method: "PATCH",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        votes_to_skip: votesToSkip,
        guest_can_pause: guestCanPause,
        code: roomCode,
      }),
    };
    fetch("/api/update-room", requestOptions)
      .then((response) => {
        if (response.ok) {
          this.setState({
            successMsg: "Room updated successfully!"
          });
        }
        else {
          this.setState({
            errorMsg: "Error updating room..."
          });
        }
        updateCallback();
      });
      
  }
  renderCreateButtons() {
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} align="center">
          <Button
            color="primary"
            variant="contained"
            onClick={this.handleCreateButtonPressed}
          >
            Create a Room
      </Button>
        </Grid>
        <Grid item xs={12} align="center">
          <Button color="secondary" variant="contained" to="/" component={Link}>
            Back
      </Button>
        </Grid>
      </Grid>);
  }

  renderUpdateButtons() {
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} align="center">
          <Button
            color="primary"
            variant="contained"
            onClick={this.handleUpdateButtonPressed}
          >
            Update Room
          </Button>
        </Grid>
        <Grid item xs={12} align="center">
          <Button color="secondary" variant="contained" to="/" component={Link}>
            Back
          </Button>
        </Grid>
      </Grid>
    )
  }

  render() {
    const { votesToSkip, errorMsg, successMsg } = this.state;
    const { update, guestCanPause } = this.props;

    const title = update ? "Update Room" : "Create a Room";
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} align="center">
          <Collapse in={errorMsg != "" || successMsg != ""}>
            {successMsg != "" 
            ? (<Alert severity="success" onClose={() => {this.setState({
              successMsg: ""
            })}} >
              {successMsg}
              </Alert>
              ) 
            : (<Alert severity="error" onClose={() => {this.setState({
              errorMsg: ""
            })}}>
              {errorMsg}
              </Alert>)
              }
          </Collapse>
        </Grid>
        <Grid item xs={12} align="center">
          <Typography component="h2" variant="h2">
            {title}
          </Typography>
        </Grid>
        <Grid item xs={12} align="center">
          <FormControl component="fieldset">
            <FormHelperText>
              <div align="center">Guest Control of Playback State</div>
            </FormHelperText>
            <RadioGroup
              row
              defaultValue={guestCanPause.toString()}
              onChange={this.handleGuestCanPauseChange}
            >
              <FormControlLabel
                value="true"
                control={<Radio color="primary" />}
                label="Play/Pause"
                labelPlacement="bottom"
              />
              <FormControlLabel
                value="false"
                control={<Radio color="secondary" />}
                label="No Control"
                labelPlacement="bottom"
              />
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12} align="center">
          <FormControl>
            <TextField
              required={true}
              type="number"
              onChange={this.handleVotesChange}
              defaultValue={this.state.votesToSkip}
              inputProps={{
                min: 1,
                style: { textAlign: "center" },
              }}
            />
            <FormHelperText>
              <div align="center">Votes Required To Skip Song</div>
            </FormHelperText>
          </FormControl>
        </Grid>
        {update
          ? this.renderUpdateButtons()
          : this.renderCreateButtons()}

      </Grid>
    );
  }
}
